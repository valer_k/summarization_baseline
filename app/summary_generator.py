import json

import gensim

from app.summarizer_v2 import Summarizer

BASE_FILE_DAILYMAIL = './data/dailymail_stories.json'
SUMMARY_FILE_DAILYMAIL = './data/dailymail_stories_summarized.json'
BASE_FILE_CNN = './data/cnn_stories.json'
SUMMARY_FILE_CNN = './data/cnn_stories_summarized.json'

summarizer = Summarizer(type='graph', bm25=True, w2v=False)

with open(file=SUMMARY_FILE_DAILYMAIL, mode='w', encoding='utf-8') as output_json:
    with open(file=BASE_FILE_DAILYMAIL, mode='r', encoding='utf-8') as input_json:

        i = 0

        for row in input_json:
            article = json.loads(row)

            summary_list = []

            try:

                summarizer.summarize(' '.join(article['story']))

                for sentence in sorted(summarizer.sentences,
                                       key=lambda sentence: sentence['pagerank_score'], reverse=True):
                    summary_list.append({
                        'pagerank_score': sentence['pagerank_score'],
                        'sentence': sentence['original_sentence']
                    })
            except ZeroDivisionError:
                continue

            article['custom_summarization'] = summary_list

            try:
                article['gensim_summarization'] = \
                    gensim.summarization.summarizer.summarize(' '.join(article['story']), split=True)
            except ValueError:
                continue

            output_json.write(
                json.dumps(article) + '\n'
            )

            i += 1

            print('Written story no.', i)

            if i > 1000:
                break

'''
for document in summarizer.summarize_text(example):
    print(document['pagerank_score'], document['sentence'])
'''