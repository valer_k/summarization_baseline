import nltk
from nltk.stem import SnowballStemmer
from nltk.corpus import stopwords

import re
import string
import math

import numpy as np
from scipy.linalg import eig
from scipy.sparse.linalg import eigs
from numpy import empty as empty_matrix

from collections import defaultdict
from six import iteritems, iterkeys, itervalues


class Summarizer:

    def __init__(self):

        self.original_sentences = []
        self.clean_sentences = []
        self.hashable_corpus = []

        self.weights = []
        self.pagerank_scores = {}

        self.final_documents = []

    def preprocess(self, text):

        STOPWORDS = stopwords.words('english')
        RE_PUNCT = re.compile(r'([%s])+' % re.escape(string.punctuation), re.UNICODE)
        RE_TAGS = re.compile(r"<([^>]+)>", re.UNICODE)
        RE_NUMERIC = re.compile(r"[0-9]+", re.UNICODE)
        RE_WHITESPACE = re.compile(r"(\s)+", re.UNICODE)

        regex_cleaners_none = [RE_TAGS, RE_NUMERIC]
        regex_cleaners_space = [RE_PUNCT, RE_WHITESPACE]

        self.original_sentences = nltk.tokenize.sent_tokenize(text)

        stemmer = SnowballStemmer('english')

        minsize = 3

        for sentence in self.original_sentences:

            sentence_temp = sentence.lower()

            for regex in regex_cleaners_space:
                sentence_temp = regex.sub(' ', sentence_temp)

            for regex in regex_cleaners_none:
                sentence_temp = regex.sub('', sentence_temp)

            sentence_temp_list = []

            for token in sentence_temp.split():

                if token not in STOPWORDS:

                    token = stemmer.stem(token)

                    if len(token) >= minsize:
                        sentence_temp_list.append(token)

            '''
            clean_sentences.append(' '.join(sentence_temp_list))
            '''
            self.clean_sentences.append(sentence_temp_list)

    def build_bag_of_words_model(self):

        corpus = []

        token2id = {}
        dfs = {}

        num_docs = 0
        num_pos = 0
        num_nnz = 0

        for clean_sentence in self.clean_sentences:
            counter = defaultdict(int)
            for w in clean_sentence:
                counter[w] += 1

            missing = sorted(x for x in iteritems(counter) if x[0] not in token2id)

            for w, _ in missing:
                token2id[w] = len(token2id)

            result = {token2id[w]: freq for w, freq in iteritems(counter) if w in token2id}

            num_docs += 1
            num_pos += sum(itervalues(counter))
            num_nnz += len(result)

            for tokenid in iterkeys(result):
                dfs[tokenid] = dfs.get(tokenid, 0) + 1

            corpus.append(sorted(iteritems(result)))

        self.hashable_corpus = [tuple(doc) for doc in corpus]

    def get_bm25_scores(self):

        corpus_size = len(self.hashable_corpus)
        doc_len = []
        frequency_list = []
        df = {}
        idf = {}

        avgdl = sum(float(len(x)) for x in self.hashable_corpus) / corpus_size

        for document in self.hashable_corpus:
            frequencies = {}
            doc_len.append(len(document))
            for word in document:
                if word not in frequencies:
                    frequencies[word] = 0
                frequencies[word] += 1
            frequency_list.append(frequencies)

            for word, freq in iteritems(frequencies):
                if word not in df:
                    df[word] = 0
                df[word] += 1

        for word, freq in iteritems(df):
            idf[word] = math.log(corpus_size - freq + 0.5) - math.log(freq + 0.5)

        average_idf = sum(float(val) for val in idf.values()) / len(idf)

        PARAM_K1 = 1.5
        PARAM_B = 0.75
        EPSILON = 0.25

        self.weights = []

        for hashable_corpus_document in self.hashable_corpus:

            scores = []

            for index in range(corpus_size):

                score = 0

                for word in hashable_corpus_document:

                    if word not in frequency_list[index]:
                        continue

                    idf_value = idf[word] if idf[word] >= 0 else EPSILON * average_idf
                    score += (idf_value * frequency_list[index][word] * (PARAM_K1 + 1)
                              / (frequency_list[index][word] + PARAM_K1 * (
                                        1 - PARAM_B + PARAM_B * doc_len[index] / avgdl)))
                scores.append(score if score >= 1.0 else 0.0)

            self.weights.append(scores)

    def build_adjacency_matrix(self):

        WEIGHT_THRESHOLD = 1.e-3

        self.adjacency_matrix = np.zeros([len(self.hashable_corpus), len(self.hashable_corpus)])

        for i in range(len(self.hashable_corpus)):
            for j in range(len(self.hashable_corpus)):
                if i == j or self.weights[i][j] < WEIGHT_THRESHOLD:
                    continue

                self.adjacency_matrix[i][j] = self.weights[i][j]

        for i in range(len(self.hashable_corpus)):
            for j in range(len(self.hashable_corpus)):
                if np.sum(self.adjacency_matrix[i]) != 0.0:
                    self.adjacency_matrix[i][j] /= np.sum(self.adjacency_matrix[i])

    def build_pagerank_matrix(self):

        dimension = len(self.hashable_corpus)
        damping = 0.85

        matrix = empty_matrix((dimension, dimension))

        probability = 1.0 / float(dimension)
        matrix.fill(probability)

        probability_matrix = matrix

        pagerank_matrix = damping * self.adjacency_matrix + (1 - damping) * probability_matrix

        if len(pagerank_matrix.T) < 3:
            vals, vecs = eig(pagerank_matrix.T)
            ind = np.abs(vals).argmax()
            vec = vecs[:, ind]
        else:
            vals, vecs = eigs(pagerank_matrix.T, k=1)
            vec = vecs[:, 0]


        for i, node in enumerate(self.hashable_corpus):
            self.pagerank_scores[node] = abs(vec.real[i])

    def get_final_documents(self):

        for idx in range(len(self.hashable_corpus)):
            self.final_documents.append({
                'corpus': self.hashable_corpus[idx],
                'sentence': self.original_sentences[idx],
                'pagerank_score': self.pagerank_scores.get(self.hashable_corpus[idx], 0)
            })

        self.final_documents.sort(key=lambda doc: doc['pagerank_score'], reverse=True)

    def summarize_text(self, text):

        self.__init__()
        self.preprocess(text)
        self.build_bag_of_words_model()
        print('Custom hashable corpus:', self.hashable_corpus)
        self.get_bm25_scores()
        print(self.weights)
        self.build_adjacency_matrix()
        '''
        print('Custom adjacency matrix:', self.adjacency_matrix[0:2])
        '''
        self.build_pagerank_matrix()
        self.get_final_documents()

        return self.final_documents

if __name__ == '__main__':

    example = "This week marks the final print copy of NME, once the engine of the hype and heat around the music industry." \
             " But its narrow musical focus in its later years doomed it to irrelevance. On one hand, the demise of " \
             "NME’s print edition seems inevitable. For all the title’s loud touting of its increased circulation " \
             "figures since it became a freesheet in September 2015 – they were apparently better than they had " \
             "been since 1964 – the paper was an irrelevant shadow of its former self. The kind of things about NME " \
             "that had once been hot topics had long ceased to be discussed, even within the music industry. " \
             "Who was on its cover that week? Who was it hyping as the new saviour of rock’n’roll? Which unfortunate " \
             "had been dealt a kicking in the reviews section by one of its bolshy star writers? If it was mentioned at " \
             "all, it was in tones of bafflement and pity. The last big furore NME caused wasn’t over an album it had " \
             "slagged off, nor a sacred cow it had taken to task in an interview, nor a band it had praised to the " \
             "point that said band was clearly doomed, incapable of fulfilling the expectations heaped on their shoulders." \
             " It was when its online wing inexplicably ran the news story: “M&S to stop selling £2.50 vegan ‘cauliflower " \
             "steaks’ following complaints.” You didn’t have to be one of those misty-eyed nostalgics for the paper’s " \
             "glory days, ever ready to trot out the impossibly well-worn stories about Nick Kent’s testicles hanging " \
             "out of his leather pants or Julie Burchill and Tony Parsons putting barbed wire round their area of the " \
             "office – stories we’re going to be given umpteen opportunities to enjoy yet again in the coming days – to" \
             " think this was a terribly depressing way for NME to go out. The truth is that the paper had umpteen " \
             "“golden eras”, and, with the greatest of respect to everyone involved in its manufacture, their existence" \
             " usually had less to do with the quality of the writing than whether or not you were 17 or 18 when you " \
             "were reading it. But regardless of whether you thought the NME’s best days involved prog or punk or Pete " \
             "Doherty, in the end it became clueless as to what it was supposed to be doing, or who it was supposed " \
             "to be for. But there is also something surprising about NME’s demise. Even its loudest detractor would " \
             "struggle to claim that it was anything other than adaptable. It had survived everything, from the rise " \
             "of rock’n’roll itself, four years after it had changed its title from the Musical Express (“incorporating " \
             "Accordion Times”), to the death of most of its rivals: it’s 18 years since Melody Maker and Select closed, " \
             "27 years since Sounds breathed its last. NME hadn’t always been at the forefront of every new genre that " \
             "appeared, and some had caused ructions and fissures to appear among its staff. Not everyone on the NME " \
             "staff was as welcoming of punk as its then editor, Neil Spencer, who fixed the 1976 readers’ poll so " \
             "that the Sex Pistols were the “brightest hope for 1977”, rather than Phil Collins’s jazz-fusion band " \
             "Brand X; a so-called “hip-hop war” erupted in the mid-80s between writers who wanted to focus on rap, soul " \
             "and club music and those who preferred to stick with what had become NME’s stock-in-trade, indie guitar " \
             "rock. But none of them actually brought about its downfall. So what did? The pat answer is the internet, " \
             "which dealt a mortal blow to print media generally, and ushered in an era where pop and rock music has ceased" \
             " to be, in the writer Jon Savage’s lovely phrase, “the teenage news” – the main means by which youth " \
             "culture defines itself, a conduit that transmitted everything from fashion to politics to a young audience. " \
             "Accordingly, music taste has become less tribal and more pluralistic than before. If there’s always some " \
             "die-hard numpty eager to get up a petition when Glastonbury books a rapper or R&B star as a headliner, there’s " \
             "a sense that most music fans don’t define themselves in narrow terms: the kind of clannish boundaries " \
             "that could once have caused a hip-hop war to erupt among NME staff no longer exist. In the 00s, NME’s " \
             "response to this was to double down on its core audience, focusing increasingly exclusively on indie " \
             "guitar rock, then enjoying a renaissance thanks to the White Stripes and the Strokes. Doing so prompted " \
             "a renaissance in sales, but when that wave of “alternative” rock ran out of steam, NME found itself without " \
             "any real musical constituency to speak of. By the turn of the decade there was something very off-putting " \
             "about how many of its covers were devoted to artists who were dead, or bands that had long split up, or " \
             "enjoyed their biggest success decades ago: the Smiths, the Clash, Syd Barrett, John Lennon, Joy Division, " \
             "the Sex Pistols, the Stone Roses. The past is more interesting than the present, was the disheartening " \
             "underlying message. Perhaps if it had paid less attention to market research and kept its musical " \
             "outlook broad, it might have survived longer, better-equipped to navigate an era in which R&B and " \
             "hip-hop are commercially and creatively dominant, and grime is the underground genre enjoying the biggest " \
             "crossover into mainstream success. Or perhaps not: the precarious climate for print media, particularly " \
             "in the music press, doesn’t really encourage any kind of risk-taking. As it is, NME finds itself exiting " \
             "the stage mourned exclusively by people old enough to remember a time when it seemed important. Sad to say, " \
             "it seems unlikely you’ll find an 18-year-old in 2018 who cares much whether it exists or not."

    summ = Summarizer()
    summ.summarize_text(example)
    '''
    for document in summ.summarize_text(example):
        print(document['pagerank_score'], document['sentence'])
    '''

    from gensim.summarization import summarizer

    print(summarizer.summarize(example, split=True))



