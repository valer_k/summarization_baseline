import csv

with open(file='../results/results_dailymail.csv', mode='w') as output_csv:

    writer = csv.writer(output_csv)

    with open(file='/home/valer/University/text_summarization/rouge/results_dailymail_summarization.csv', mode='r') as input_csv:

        reader = csv.reader(input_csv)

        i = 0

        for row in reader:

            if i == 0:
                writer.writerow(row)


                i += 1

                continue

            if len(row) == 0:
                continue

            try:
                avg_recall = float(row[3] + '.' + row[4])
                avg_precision = float(row[5] + '.' + row[6])
                avg_fscore = float(row[7] + '.' + row[8])
                results_row = row[:3] + [avg_recall] + [avg_precision] + [avg_fscore] + [row[-1]]

                writer.writerow(results_row)
            except:
                pass

            i += 1

            print('Processed result row no.', i)
