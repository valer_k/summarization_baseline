import csv

import numpy as np

gensim_recall = []
custom_recall = []
gensim_precision = []
custom_precision = []
gensim_fscores = []
custom_fscores = []

with open(file='../results/results_dailymail.csv', mode='r') as input_csv:
    reader = csv.DictReader(input_csv)

    for row in reader:

        if row['System Name'] == 'GENSIM.TXT':

            gensim_recall.append(float(row['Avg_Recall']))
            gensim_precision.append(float(row['Avg_Precision']))
            gensim_fscores.append(float(row['Avg_F-Score']))

        elif row['System Name'] == 'CUSTOM.TXT':

            custom_recall.append(float(row['Avg_Recall']))
            custom_precision.append(float(row['Avg_Precision']))
            custom_fscores.append(float(row['Avg_F-Score']))

print('Average recall for gensim:', np.average(gensim_recall))
print('Average recall for custom:', np.average(custom_recall))
print('Average precision for gensim:', np.average(gensim_precision))
print('Average precision for custom:', np.average(custom_precision))
print('Average f-score for gensim:', np.average(gensim_fscores))
print('Average f-score for custom:', np.average(custom_fscores))

'''
Prune Max BM25:
Average recall for gensim: 0.417826478978979
Average recall for custom: 0.45813740240240247
Average precision for gensim: 0.12555552052052052
Average precision for custom: 0.11923363863863863
Average f-score for gensim: 0.18000962212212213
Average f-score for custom: 0.18068244244244244

Prune Max W2V:
Average recall for gensim: 0.417826478978979
Average recall for custom: 0.45239733733733734
Average precision for gensim: 0.12555552052052052
Average precision for custom: 0.10583095595595596
Average f-score for gensim: 0.18000962212212213
Average f-score for custom: 0.16426502002002003

Prune max combined W2V and BM25:
Average recall for gensim: 0.417826478978979
Average recall for custom: 0.46390926426426426
Average precision for gensim: 0.12555552052052052
Average precision for custom: 0.1125190015015015
Average f-score for gensim: 0.18000962212212213
Average f-score for custom: 0.17327111111111113

Prune min combined W2V and BM25:
Average recall for gensim: 0.417826478978979
Average recall for custom: 0.40927631381381385
Average precision for gensim: 0.12555552052052052
Average precision for custom: 0.11376367367367368
Average f-score for gensim: 0.18000962212212213
Average f-score for custom: 0.1687386961961962
'''