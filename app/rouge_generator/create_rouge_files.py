import json
import math
import re

SCORE_LIMIT = 0.7

SUMMARY_FILE_DAILYMAIL = '../data/dailymail_stories_summarized.json'

with open(file=SUMMARY_FILE_DAILYMAIL, mode='r', encoding='utf-8') as input_json:

    story_count = 1

    for row in input_json:

        story_dict = json.loads(row)

        if len(story_dict['summary']) == 0 or len(story_dict['gensim_summarization']) == 0 or \
            len(story_dict['custom_summarization']) == 0:
            continue

        with open(file='/home/valer/University/text_summarization/rouge/projects/dailymail/reference/story' +
                  str(story_count) + '_reference.txt', mode='w', encoding='utf-8') as output_story:

            for sentence in story_dict['summary']:
                output_story.write(re.sub('_', '', sentence) + '\n')

        with open(file='/home/valer/University/text_summarization/rouge/projects/dailymail/system/story' +
                       str(story_count) + '_gensim.txt', mode='w', encoding='utf-8') as output_story:

            for sentence in story_dict['gensim_summarization']:
                output_story.write(re.sub('_', '', sentence) + '\n')

        with open(file='/home/valer/University/text_summarization/rouge/projects/dailymail/system/story' +
                       str(story_count) + '_custom.txt', mode='w', encoding='utf-8') as output_story:
            '''
            score_limit = 0.0

            for sentence in story_dict['custom_summarization']:
                score_limit += sentence['pagerank_score']

            score_limit *= SCORE_LIMIT

            score_sum = 0.0

            for sentence in story_dict['custom_summarization']:
                score_sum += sentence['pagerank_score']

                output_story.write(re.sub('_', '', sentence['sentence']) + '\n')

                if score_sum >= score_limit:
                    break
            '''
            for sentence in story_dict['custom_summarization'][0:math.ceil(0.2 * len(story_dict['custom_summarization']))]:
                output_story.write(re.sub('_', '', sentence['sentence']) + '\n')


        print('Processed story no.', story_count)

        story_count += 1

