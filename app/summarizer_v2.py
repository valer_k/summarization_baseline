from app.preprocessor.preprocessor import Preprocessor
from app.preprocessor.bag_of_words_model import bag_of_words_model
from app.bm25_scorer.bm25_scorer import bm25_scorer
from app.word2vec_scorer.word2vec_scorer import word2vec_scorer
from app.pagerank.pagerank import Pagerank
from app.graph_summarizer.graph_summarizer import GraphSummarizer

class Summarizer:

    def __init__(self, type='graph', graph_method='product', bm25=True, w2v=False, w2v_score_type='combined'):

        self.preprocessor = Preprocessor()
        self.bow = bag_of_words_model()

        self.scorers = []

        if w2v:
            if w2v_score_type == 'distance' or w2v_score_type == 'combined':
                self.scorers.append(
                    word2vec_scorer(
                        w2v_model_path='/home/valer/University/text_summarization/summarization_scripts/app/w2v_models/w2v_dailymail_epoch20_size100_window8_mincount5',
                        score_type='distance'
                    )
                )

            if w2v_score_type == 'cosine' or w2v_score_type == 'combined':
                self.scorers.append(
                    word2vec_scorer(
                        w2v_model_path='/home/valer/University/text_summarization/summarization_scripts/app/w2v_models/w2v_dailymail_epoch20_size100_window8_mincount5',
                        score_type='cosines'
                    )
                )

        if bm25:
            self.scorers.append(bm25_scorer())

        if type == 'pagerank':
            self.ranker = Pagerank()
        else:
            self.ranker = GraphSummarizer(method=graph_method)

        self.sentences = []

    def summarize(self, text):

        self.preprocessor.preprocess(text=text)
        self.bow.build_model(self.preprocessor.sentences)

        sentences = []

        for scorer in self.scorers:
            scorer.get_scores(self.bow.sentences)

            sentences.append(scorer.sentences)

        self.ranker.rank_calculation(sentences)

        self.sentences = self.ranker.sentences


if __name__ == '__main__':

    example = "This week marks the final print copy of NME, once the engine of the hype and heat around the music industry." \
              " But its narrow musical focus in its later years doomed it to irrelevance. On one hand, the demise of " \
              "NME’s print edition seems inevitable. For all the title’s loud touting of its increased circulation " \
              "figures since it became a freesheet in September 2015 – they were apparently better than they had " \
              "been since 1964 – the paper was an irrelevant shadow of its former self. The kind of things about NME " \
              "that had once been hot topics had long ceased to be discussed, even within the music industry. " \
              "Who was on its cover that week? Who was it hyping as the new saviour of rock’n’roll? Which unfortunate " \
              "had been dealt a kicking in the reviews section by one of its bolshy star writers? If it was mentioned at " \
              "all, it was in tones of bafflement and pity. The last big furore NME caused wasn’t over an album it had " \
              "slagged off, nor a sacred cow it had taken to task in an interview, nor a band it had praised to the " \
              "point that said band was clearly doomed, incapable of fulfilling the expectations heaped on their shoulders." \
              " It was when its online wing inexplicably ran the news story: “M&S to stop selling £2.50 vegan ‘cauliflower " \
              "steaks’ following complaints.” You didn’t have to be one of those misty-eyed nostalgics for the paper’s " \
              "glory days, ever ready to trot out the impossibly well-worn stories about Nick Kent’s testicles hanging " \
              "out of his leather pants or Julie Burchill and Tony Parsons putting barbed wire round their area of the " \
              "office – stories we’re going to be given umpteen opportunities to enjoy yet again in the coming days – to" \
              " think this was a terribly depressing way for NME to go out. The truth is that the paper had umpteen " \
              "“golden eras”, and, with the greatest of respect to everyone involved in its manufacture, their existence" \
              " usually had less to do with the quality of the writing than whether or not you were 17 or 18 when you " \
              "were reading it. But regardless of whether you thought the NME’s best days involved prog or punk or Pete " \
              "Doherty, in the end it became clueless as to what it was supposed to be doing, or who it was supposed " \
              "to be for. But there is also something surprising about NME’s demise. Even its loudest detractor would " \
              "struggle to claim that it was anything other than adaptable. It had survived everything, from the rise " \
              "of rock’n’roll itself, four years after it had changed its title from the Musical Express (“incorporating " \
              "Accordion Times”), to the death of most of its rivals: it’s 18 years since Melody Maker and Select closed, " \
              "27 years since Sounds breathed its last. NME hadn’t always been at the forefront of every new genre that " \
              "appeared, and some had caused ructions and fissures to appear among its staff. Not everyone on the NME " \
              "staff was as welcoming of punk as its then editor, Neil Spencer, who fixed the 1976 readers’ poll so " \
              "that the Sex Pistols were the “brightest hope for 1977”, rather than Phil Collins’s jazz-fusion band " \
              "Brand X; a so-called “hip-hop war” erupted in the mid-80s between writers who wanted to focus on rap, soul " \
              "and club music and those who preferred to stick with what had become NME’s stock-in-trade, indie guitar " \
              "rock. But none of them actually brought about its downfall. So what did? The pat answer is the internet, " \
              "which dealt a mortal blow to print media generally, and ushered in an era where pop and rock music has ceased" \
              " to be, in the writer Jon Savage’s lovely phrase, “the teenage news” – the main means by which youth " \
              "culture defines itself, a conduit that transmitted everything from fashion to politics to a young audience. " \
              "Accordingly, music taste has become less tribal and more pluralistic than before. If there’s always some " \
              "die-hard numpty eager to get up a petition when Glastonbury books a rapper or R&B star as a headliner, there’s " \
              "a sense that most music fans don’t define themselves in narrow terms: the kind of clannish boundaries " \
              "that could once have caused a hip-hop war to erupt among NME staff no longer exist. In the 00s, NME’s " \
              "response to this was to double down on its core audience, focusing increasingly exclusively on indie " \
              "guitar rock, then enjoying a renaissance thanks to the White Stripes and the Strokes. Doing so prompted " \
              "a renaissance in sales, but when that wave of “alternative” rock ran out of steam, NME found itself without " \
              "any real musical constituency to speak of. By the turn of the decade there was something very off-putting " \
              "about how many of its covers were devoted to artists who were dead, or bands that had long split up, or " \
              "enjoyed their biggest success decades ago: the Smiths, the Clash, Syd Barrett, John Lennon, Joy Division, " \
              "the Sex Pistols, the Stone Roses. The past is more interesting than the present, was the disheartening " \
              "underlying message. Perhaps if it had paid less attention to market research and kept its musical " \
              "outlook broad, it might have survived longer, better-equipped to navigate an era in which R&B and " \
              "hip-hop are commercially and creatively dominant, and grime is the underground genre enjoying the biggest " \
              "crossover into mainstream success. Or perhaps not: the precarious climate for print media, particularly " \
              "in the music press, doesn’t really encourage any kind of risk-taking. As it is, NME finds itself exiting " \
              "the stage mourned exclusively by people old enough to remember a time when it seemed important. Sad to say, " \
              "it seems unlikely you’ll find an 18-year-old in 2018 who cares much whether it exists or not."

    summ = Summarizer(type='graph', bm25=True, w2v=False)
    summ.summarize(text=example)

    print('Text 1:')

    for sentence in sorted(summ.sentences, key=lambda sentence: sentence['pagerank_score'], reverse=True):
        print(sentence['pagerank_score'], sentence['original_sentence'])

    example = "The computer on which this article was written is sitting on a laptop stand that tells you everything you" \
              " need to know about how Amazon does business. At $19.99 (£14.99) a pop, the laptop stand combines everything" \
              " customers love about Amazon: utility, price and convenience. It’s also a total and complete knockoff – of a" \
              " laptop stand that the San Francisco-based company Rain Design began selling nearly a decade before Amazon" \
              " decided to make its own. Amazon’s innovation with its own version was to replace Rain Design’s raindrop logo" \
              " with its own smiley arrow logo – and cut the price in half. “All Amazon had to do was pick the best one " \
              "and copy it,” said Rachel Greer, a former product manager for Amazon who runs a consulting firm for Amazon" \
              " vendors. Rain Design isn’t the first company to fall victim to the aggressive techniques Amazon uses " \
              "to achieve market dominance. Although its retail site is the most visible of its business strands, the " \
              "$740bn company has quietly stretched its tentacles into an astonishing range of unrelated industries. " \
              "Google and Facebook might have cornered the online advertising market, but Amazon’s business successes " \
              "now include groceries, TV, robotics, cloud services and consumer electronics. “If you try to measure power " \
              "by how many executives are up at night because of X company, I think Amazon would win,” said Lina Khan, " \
              "legal fellow with the Open Markets Program at the thinktank New America. Amazon has a restaurant delivery" \
              " service, a music streaming service and an Etsy clone called Amazon Homemade. It makes hugely successful" \
              " hardware and software; it makes movies, television shows and video games. It runs a labour brokerage for" \
              " computer-based work and another for manual labour. It publishes books, sells books, and owns the popular" \
              " social network site for book readers GoodReads.com. It sells diapers, baby food, snacks, clothing, " \
              "furniture and batteries. It sells ads, processes payments, and makes small loans. It is the unexpected owner" \
              " of a huge number of websites – everything from the gaming livestream site Twitch to the movie database " \
              "IMDb. Of the top 10 US industries by GDP (information, manufacturing non-durable goods, retail trade, " \
              "wholesale trade, manufacturing durable goods, healthcare, finance and insurance, state and local government," \
              " professional and business services, and real estate), Amazon has a finger in all but real estate. And how" \
              " confident can the real estate industry be right now that Amazon won’t at some point decide to allow people" \
              " to buy and sell homes on its platform? “I see them as kind of a great white shark,” said Greer. “You don’t" \
              " really want to mess with them.” “It’s basically become a railroad for the 21st century,” added Khan." \
              " “It’s existential for so many businesses but also competing with all those businesses.” What makes Amazon" \
              " so frightening for rival businesses is that it can use its expertise in data analytics to move into almost" \
              " any sector. “Amazon has all this data available. They track what people are searching for, what they " \
              "click, what they don’t,” said Greer. “Every time you’re searching for something and don’t click, you’re " \
              "telling Amazon that there’s a gap.” Amazon knows where you live, who you live with, your current " \
              "location (if you use an Amazon smartphone app), what TV shows you watch, what music you listen to and what" \
              " websites you visit. “It’s obsessed with understanding its customers,” said the Tuck School of Business" \
              " professor Vijay Govindarajan. “The more you understand every aspect of a customer, the more it can " \
              "satisfy your needs.” This obsession with knowing its customers means that, while feared by almost every " \
              "business, Amazon is beloved by customers. It’s viewed as cheap, convenient and reliable. “Amazon’s north " \
              "star is to delight the customer,” said Gene Munster, a former Amazon analyst who runs an investment firm," \
              " Loup Ventures. Despite its treasure trove of personal data – the kind that allows retailers to predict" \
              " if their teenage customers are pregnant before their parents know – Amazon has mostly avoided using that" \
              " data in ways that unnerve people. But the consumer trust it has built up does not reflect the damage " \
              "the company does to competitors, partners and workers, according to Khan. “Only looking at a consumer " \
              "side of a business power is totally ludicrous. It slices the human in half, not looking at them as a " \
              "worker, producer or supplier.” Customers might be getting super-fast deliveries of cheap laundry detergent" \
              " and binge-worthy TV shows, but the same company has also been accused of displacing jobs in the locations" \
              " where it builds its fulfilment centres, treating warehouse workers like robots, aggressively undercutting" \
              " rivals and squeezing suppliers and producers. “The algorithms are designed to serve up things that best " \
              "serve Amazon, steering us to some books and not others,” said Stacy Mitchell, co-director of the Institute" \
              " for Local Self-Reliance. “You have a company that can shape whether a particular author is able to find " \
              "an audience, and whether they can even get published.” That power means that “people don’t know if there’s" \
              " something they’re missing”. The company doesn’t even shy away from competing with its own investments." \
              " After pumping $5.6m into the startup Nucleus and its Alexa-powered video-conferencing tablet, Amazon turned" \
              " around and released its own suspiciously similar device, the Echo Show. “They probably copied us,” the " \
              "Nucleus co-founder Jonathan Frankel said last year. “When they had the opportunity to extend their tentacles " \
              "into millions of homes, they had to do it, even if it means throwing us under the bus, even if it means putting" \
              " their whole ecosystem at risk and letting people know that they’re not necessarily a trusted partner.” Amazon" \
              " declined to comment on its investment into Nucleus or on the design of its laptop stand, but noted that " \
              "Rain’s design remained the bestselling stand on the site. The company’s success has produced panic among" \
              " investors. When Amazon bought Whole Foods, grocery chains’ stock prices crashed. Two months later, when" \
              " Amazon announced it would cut Whole Foods’ prices, grocery stocks plummeted again. The meal kit maker Blue" \
              " Apron’s stock price fell 11% after the news that Amazon was filing for a meal kit trademark. A vague " \
              "announcement from Amazon that it was collaborating with JP Morgan and Berkshire Hathaway on some kind of " \
              "non-profit healthcare venture sent healthcare stocks on a downward slide. Since 2012, Bespoke Investment" \
              " Group has been tracking an index of 54 retail stocks, known as the “Death by Amazon index”, that it " \
              "considers most vulnerable to Amazon. “It’s a somewhat melodramatic title, we admit,” said George Pearkes," \
              " a macro strategist with Bespoke. “But it encapsulates what is going on in retail quite well.” Between February" \
              " 2012 and January 2018, Amazon’s value rose 560%, the S&P index rose 102%, and the Death by Amazon index " \
              "grew just 42.8%. Amazon still has a lot of room to grow. It dominates e-commerce, but that’s only about " \
              "9%, (according to eMarketer) of the total retail market in the US. With the acquisition of Whole Foods " \
              "and the launch of the concept store Amazon Go – which has no cashiers and no checkouts – the tech giant can" \
              " start to take on the other 91%. And it wouldn’t be surprising if Amazon were to sell the technology that" \
              " powers the futuristic stores to other retailers so that they too could automate their stores and cut " \
              "jobs. Doing that would allow the company to keep track of the sales made by its competitors – just as it " \
              "does on Amazon.com – and use that data to inform its decisions about other retail categories to move into. " \
              "“Amazon is just getting started,” said Govindarajan. Another huge growth area is healthcare and the " \
              "pharmacy business, according to Khan, who sees the health insurance joint-venture Amazon launched with " \
              "Berkshire Hathaway and JP Morgan as a way to “get experience in the sector and then double down”. The " \
              "company, which is already taking payments and making loans to third-party sellers, has also laid the foundations" \
              " to push further into financial services. “It wouldn’t surprise me if they tried to look at the option of " \
              "obtaining an industrial bank charter,” said Mitchell. What’s the CEO Jeff Bezos’s endgame? Khan suggests it " \
              "could be a “tax on all economic activity”. “If you view it that way it’s difficult to think about what " \
              "sector he wouldn’t touch,” she said. Khan believes that Amazon needs to face antitrust regulation, " \
              "but current law is not equipped to deal with it. “I think a rule that prohibited it from competing with " \
              "the businesses that use its platforms would eliminate a lot of the core conflicts I mentioned,” she said. " \
              "Eliminating this conflict of interest would mean Amazon could provide the Amazon.com marketplace, but " \
              "not its products, for example. Without regulation, Amazon will “continue to extract wealth that other " \
              "businesses are creating”, Khan added."

    print('Text 2:')

    example = ' '.join([
        "A Turkish bus driver filmed dancing in the aisle with his passengers while his vehicle was doing 70 mph down a motorway has been banned from driving for two years.",
        "Metin Kandemir was arrested after a video of his antics on the bus carrying passengers through the western city of Istanbul became a hit on social media.",
        "In the clip Kandemir is seen singing and clapping wildly along to a fast paced local folk tune being played on the radio while his passengers whoop along enthusiastically.",
        "Scroll down for video ",
        "Turkish bus driver Metin Kandemir was filmed dancing in the aisle with his passengers while his vehicle was doing 70 mph down a motorway",
        "After initially just dancing behind the wheel, Kandemir stood up and held hands with a passenger while dancing with her in the aisle ",
        "On two occasions Kandemir gets out of his seat and walks into the aisle where he holds hands with a passenger as they dance together.",
        "Police arrested Kandemir after the video - shot by one his passangers - became one of the most popular on social media in Turkey.   ",
        "The 34-year-old later apologised, saying: 'It was a mistake. I apologise to everyone. I will not repeat it. The road was empty at the time. This is the first time I've ever done it.'",
        "But after hearing he had 32 previous convictions for traffic offences, Turkish judges fined him \u00a345 for reckless driving and revoked his bus driver's licence for two years.",
        "A court heard that the 34-year-old had 32 previous convictions for traffic offences",
        "Kandemir said the incident 'was a mistake' that he would not repeat, but said the 'road was empty at the time'",
        "Police spokesman Kaan Tore said: 'He's lucky not to be in prison. He could have killed everyone in that bus and worse.'",
        "Turkey has one of the worst road safety records in the world largely based on the fact that many drivers believe it is out of their control if they have an accident.",
        "The belief - known as Kismet - is that when a person's time is up, that is it, and therefore there is no point refraining from drinking while driving, maintaining the basic mechanical standards on a vehicle, or even paying attention while driving.",
        " "])

    summ.summarize(text=example)

    for sentence in sorted(summ.sentences, key=lambda sentence: sentence['pagerank_score'], reverse=True):
        print(sentence['pagerank_score'], sentence['original_sentence'])




