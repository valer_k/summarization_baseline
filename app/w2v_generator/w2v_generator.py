import gensim
import json, logging

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

from app.preprocessor.preprocessor import Preprocessor

BASE_FILE = '../data/dailymail_stories.json'

class text_iterator:

    def __init__(self, file_path):

        self.file_path = file_path

        self.preprocessor = Preprocessor()

    def __iter__(self):

        with open(file=BASE_FILE, mode='r') as input_json:

            for row in input_json:
                self.preprocessor.preprocess(
                    ' '.join(
                        json.loads(row.replace('\n', ''))['story']
                    )
                )

                for sentence in self.preprocessor.sentences:

                    if len(sentence) < 3:
                        continue

                    yield sentence['clean_sentence']

texts = text_iterator(file_path=BASE_FILE)
w2v_model = gensim.models.Word2Vec(texts, iter=20, size=100, window=8, min_count=5)
w2v_model.save('../w2v_models/w2v_dailymail_epoch20_size100_window8_mincount5')


