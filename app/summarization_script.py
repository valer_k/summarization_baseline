import nltk
import re


text_1 = "This week marks the final print copy of NME, once the engine of the hype and heat around the music industry." \
         " But its narrow musical focus in its later years doomed it to irrelevance. On one hand, the demise of " \
         "NME’s print edition seems inevitable. For all the title’s loud touting of its increased circulation " \
         "figures since it became a freesheet in September 2015 – they were apparently better than they had " \
         "been since 1964 – the paper was an irrelevant shadow of its former self. The kind of things about NME " \
         "that had once been hot topics had long ceased to be discussed, even within the music industry. " \
         "Who was on its cover that week? Who was it hyping as the new saviour of rock’n’roll? Which unfortunate " \
         "had been dealt a kicking in the reviews section by one of its bolshy star writers? If it was mentioned at " \
         "all, it was in tones of bafflement and pity. The last big furore NME caused wasn’t over an album it had " \
         "slagged off, nor a sacred cow it had taken to task in an interview, nor a band it had praised to the " \
         "point that said band was clearly doomed, incapable of fulfilling the expectations heaped on their shoulders." \
         " It was when its online wing inexplicably ran the news story: “M&S to stop selling £2.50 vegan ‘cauliflower " \
         "steaks’ following complaints.” You didn’t have to be one of those misty-eyed nostalgics for the paper’s " \
         "glory days, ever ready to trot out the impossibly well-worn stories about Nick Kent’s testicles hanging " \
         "out of his leather pants or Julie Burchill and Tony Parsons putting barbed wire round their area of the " \
         "office – stories we’re going to be given umpteen opportunities to enjoy yet again in the coming days – to" \
         " think this was a terribly depressing way for NME to go out. The truth is that the paper had umpteen " \
         "“golden eras”, and, with the greatest of respect to everyone involved in its manufacture, their existence" \
         " usually had less to do with the quality of the writing than whether or not you were 17 or 18 when you " \
         "were reading it. But regardless of whether you thought the NME’s best days involved prog or punk or Pete " \
         "Doherty, in the end it became clueless as to what it was supposed to be doing, or who it was supposed " \
         "to be for. But there is also something surprising about NME’s demise. Even its loudest detractor would " \
         "struggle to claim that it was anything other than adaptable. It had survived everything, from the rise " \
         "of rock’n’roll itself, four years after it had changed its title from the Musical Express (“incorporating " \
         "Accordion Times”), to the death of most of its rivals: it’s 18 years since Melody Maker and Select closed, " \
         "27 years since Sounds breathed its last. NME hadn’t always been at the forefront of every new genre that " \
         "appeared, and some had caused ructions and fissures to appear among its staff. Not everyone on the NME " \
         "staff was as welcoming of punk as its then editor, Neil Spencer, who fixed the 1976 readers’ poll so " \
         "that the Sex Pistols were the “brightest hope for 1977”, rather than Phil Collins’s jazz-fusion band " \
         "Brand X; a so-called “hip-hop war” erupted in the mid-80s between writers who wanted to focus on rap, soul " \
         "and club music and those who preferred to stick with what had become NME’s stock-in-trade, indie guitar " \
         "rock. But none of them actually brought about its downfall. So what did? The pat answer is the internet, " \
         "which dealt a mortal blow to print media generally, and ushered in an era where pop and rock music has ceased" \
         " to be, in the writer Jon Savage’s lovely phrase, “the teenage news” – the main means by which youth " \
         "culture defines itself, a conduit that transmitted everything from fashion to politics to a young audience. " \
         "Accordingly, music taste has become less tribal and more pluralistic than before. If there’s always some " \
         "die-hard numpty eager to get up a petition when Glastonbury books a rapper or R&B star as a headliner, there’s " \
         "a sense that most music fans don’t define themselves in narrow terms: the kind of clannish boundaries " \
         "that could once have caused a hip-hop war to erupt among NME staff no longer exist. In the 00s, NME’s " \
         "response to this was to double down on its core audience, focusing increasingly exclusively on indie " \
         "guitar rock, then enjoying a renaissance thanks to the White Stripes and the Strokes. Doing so prompted " \
         "a renaissance in sales, but when that wave of “alternative” rock ran out of steam, NME found itself without " \
         "any real musical constituency to speak of. By the turn of the decade there was something very off-putting " \
         "about how many of its covers were devoted to artists who were dead, or bands that had long split up, or " \
         "enjoyed their biggest success decades ago: the Smiths, the Clash, Syd Barrett, John Lennon, Joy Division, " \
         "the Sex Pistols, the Stone Roses. The past is more interesting than the present, was the disheartening " \
         "underlying message. Perhaps if it had paid less attention to market research and kept its musical " \
         "outlook broad, it might have survived longer, better-equipped to navigate an era in which R&B and " \
         "hip-hop are commercially and creatively dominant, and grime is the underground genre enjoying the biggest " \
         "crossover into mainstream success. Or perhaps not: the precarious climate for print media, particularly " \
         "in the music press, doesn’t really encourage any kind of risk-taking. As it is, NME finds itself exiting " \
         "the stage mourned exclusively by people old enough to remember a time when it seemed important. Sad to say, " \
         "it seems unlikely you’ll find an 18-year-old in 2018 who cares much whether it exists or not."


original_sentences = nltk.tokenize.sent_tokenize(text_1)

import string

RE_PUNCT = re.compile(r'([%s])+' % re.escape(string.punctuation), re.UNICODE)
RE_TAGS = re.compile(r"<([^>]+)>", re.UNICODE)
RE_NUMERIC = re.compile(r"[0-9]+", re.UNICODE)
RE_WHITESPACE = re.compile(r"(\s)+", re.UNICODE)

regex_cleaners_none = [RE_TAGS, RE_NUMERIC]
regex_cleaners_space = [RE_PUNCT, RE_WHITESPACE]

STOPWORDS = frozenset([
    'all', 'six', 'just', 'less', 'being', 'indeed', 'over', 'move', 'anyway', 'four', 'not', 'own', 'through',
    'using', 'fify', 'where', 'mill', 'only', 'find', 'before', 'one', 'whose', 'system', 'how', 'somewhere',
    'much', 'thick', 'show', 'had', 'enough', 'should', 'to', 'must', 'whom', 'seeming', 'yourselves', 'under',
    'ours', 'two', 'has', 'might', 'thereafter', 'latterly', 'do', 'them', 'his', 'around', 'than', 'get', 'very',
    'de', 'none', 'cannot', 'every', 'un', 'they', 'front', 'during', 'thus', 'now', 'him', 'nor', 'name', 'regarding',
    'several', 'hereafter', 'did', 'always', 'who', 'didn', 'whither', 'this', 'someone', 'either', 'each', 'become',
    'thereupon', 'sometime', 'side', 'towards', 'therein', 'twelve', 'because', 'often', 'ten', 'our', 'doing', 'km',
    'eg', 'some', 'back', 'used', 'up', 'go', 'namely', 'computer', 'are', 'further', 'beyond', 'ourselves', 'yet',
    'out', 'even', 'will', 'what', 'still', 'for', 'bottom', 'mine', 'since', 'please', 'forty', 'per', 'its',
    'everything', 'behind', 'does', 'various', 'above', 'between', 'it', 'neither', 'seemed', 'ever', 'across', 'she',
    'somehow', 'be', 'we', 'full', 'never', 'sixty', 'however', 'here', 'otherwise', 'were', 'whereupon', 'nowhere',
    'although', 'found', 'alone', 're', 'along', 'quite', 'fifteen', 'by', 'both', 'about', 'last', 'would',
    'anything', 'via', 'many', 'could', 'thence', 'put', 'against', 'keep', 'etc', 'amount', 'became', 'ltd', 'hence',
    'onto', 'or', 'con', 'among', 'already', 'co', 'afterwards', 'formerly', 'within', 'seems', 'into', 'others',
    'while', 'whatever', 'except', 'down', 'hers', 'everyone', 'done', 'least', 'another', 'whoever', 'moreover',
    'couldnt', 'throughout', 'anyhow', 'yourself', 'three', 'from', 'her', 'few', 'together', 'top', 'there', 'due',
    'been', 'next', 'anyone', 'eleven', 'cry', 'call', 'therefore', 'interest', 'then', 'thru', 'themselves',
    'hundred', 'really', 'sincere', 'empty', 'more', 'himself', 'elsewhere', 'mostly', 'on', 'fire', 'am', 'becoming',
    'hereby', 'amongst', 'else', 'part', 'everywhere', 'too', 'kg', 'herself', 'former', 'those', 'he', 'me', 'myself',
    'made', 'twenty', 'these', 'was', 'bill', 'cant', 'us', 'until', 'besides', 'nevertheless', 'below', 'anywhere',
    'nine', 'can', 'whether', 'of', 'your', 'toward', 'my', 'say', 'something', 'and', 'whereafter', 'whenever',
    'give', 'almost', 'wherever', 'is', 'describe', 'beforehand', 'herein', 'doesn', 'an', 'as', 'itself', 'at',
    'have', 'in', 'seem', 'whence', 'ie', 'any', 'fill', 'again', 'hasnt', 'inc', 'thereby', 'thin', 'no', 'perhaps',
    'latter', 'meanwhile', 'when', 'detail', 'same', 'wherein', 'beside', 'also', 'that', 'other', 'take', 'which',
    'becomes', 'you', 'if', 'nobody', 'unless', 'whereas', 'see', 'though', 'may', 'after', 'upon', 'most', 'hereupon',
    'eight', 'but', 'serious', 'nothing', 'such', 'why', 'off', 'a', 'don', 'whereby', 'third', 'i', 'whole', 'noone',
    'sometimes', 'well', 'amoungst', 'yours', 'their', 'rather', 'without', 'so', 'five', 'the', 'first', 'with',
    'make', 'once'
])

from nltk.stem import SnowballStemmer


stemmer = SnowballStemmer('english')

clean_sentences = []

minsize = 3

for sentence in original_sentences:

    sentence_temp = sentence.lower()

    for regex in regex_cleaners_space:
        sentence_temp = regex.sub(' ', sentence_temp)

    for regex in regex_cleaners_none:
        sentence_temp = regex.sub('', sentence_temp)

    sentence_temp_list = []

    for token in sentence_temp.split():

        if token not in STOPWORDS:

            token = stemmer.stem(token)

            if len(token) >= minsize:
                sentence_temp_list.append(token)

    '''
    clean_sentences.append(' '.join(sentence_temp_list))
    '''
    clean_sentences.append(sentence_temp_list)

from collections import defaultdict
from six import iteritems, iterkeys, itervalues

corpus = []

token2id = {}
dfs = {}

num_docs = 0
num_pos = 0
num_nnz = 0

for clean_sentence in clean_sentences:
    counter = defaultdict(int)
    for w in clean_sentence:
        counter[w] += 1

    missing = sorted(x for x in iteritems(counter) if x[0] not in token2id)

    for w, _ in missing:
        token2id[w] = len(token2id)

    result = {token2id[w]: freq for w, freq in iteritems(counter) if w in token2id}

    num_docs += 1
    num_pos += sum(itervalues(counter))
    num_nnz += len(result)

    for tokenid in iterkeys(result):
        dfs[tokenid] = dfs.get(tokenid, 0) + 1

    corpus.append(sorted(iteritems(result)))

hashable_corpus = [tuple(doc) for doc in corpus]

'''
BME25 scores
'''

import math

corpus_size = len(hashable_corpus)
doc_len = []
frequency_list = []
df = {}
idf = {}

avgdl = sum(float(len(x)) for x in hashable_corpus) / corpus_size

for document in hashable_corpus:
    frequencies = {}
    doc_len.append(len(document))
    for word in document:
        if word not in frequencies:
            frequencies[word] = 0
        frequencies[word] += 1
    frequency_list.append(frequencies)

    for word, freq in iteritems(frequencies):
        if word not in df:
            df[word] = 0
        df[word] += 1

for word, freq in iteritems(df):
    idf[word] = math.log(corpus_size - freq + 0.5) - math.log(freq + 0.5)

average_idf = sum(float(val) for val in idf.values()) / len(idf)

PARAM_K1 = 1.5
PARAM_B = 0.75
EPSILON = 0.25


weights = []

for hashable_corpus_document in hashable_corpus:

    scores = []
    '''
    scores = bm25.get_scores(doc, average_idf)
    '''
    for index in range(corpus_size):
        '''
        score = self.get_score(document, index, average_idf)
        '''

        score = 0

        for word in hashable_corpus_document:
            if word not in frequency_list[index]:
                continue

            idf_value = idf[word] if idf[word] >= 0 else EPSILON * average_idf
            score += (idf_value * frequency_list[index][word] * (PARAM_K1 + 1)
                      / (frequency_list[index][word] + PARAM_K1 * (1 - PARAM_B + PARAM_B * doc_len[index] / avgdl)))
        scores.append(score)


    weights.append(scores)

import numpy as np

WEIGHT_THRESHOLD = 1.e-3

adjacency_matrix = np.zeros([len(hashable_corpus), len(hashable_corpus)])

for i in range(len(hashable_corpus)):
    for j in range(len(hashable_corpus)):
        if i == j or weights[i][j] < WEIGHT_THRESHOLD:
            continue

        adjacency_matrix[i][j] = weights[i][j]

for i in range(len(hashable_corpus)):
    for j in range(len(hashable_corpus)):
        if np.sum(adjacency_matrix[i]) != 0.0:
            adjacency_matrix[i][j] /= np.sum(adjacency_matrix[i])

from scipy.sparse import csr_matrix
from scipy.linalg import eig
from scipy.sparse.linalg import eigs
from numpy import empty as empty_matrix

'''
adjacency_matrix = csr_matrix(adjacency_matrix)
'''

dimension = len(hashable_corpus)
damping = 0.85

matrix = empty_matrix((dimension, dimension))

probability = 1.0 / float(dimension)
matrix.fill(probability)

probability_matrix = matrix

pagerank_matrix = damping * adjacency_matrix + (1 - damping) * probability_matrix

if len(pagerank_matrix.T) < 3:
    vals, vecs = eig(pagerank_matrix.T)
    ind = np.abs(vals).argmax()
    vec = vecs[:, ind]
else:
    vals, vecs = eigs(pagerank_matrix.T, k=1)
    vec = vecs[:, 0]

pagerank_scores = {}
for i, node in enumerate(hashable_corpus):
    pagerank_scores[node] = abs(vec.real[i])


final_documents = []

for idx in range(len(hashable_corpus)):
    final_documents.append({
        'corpus': hashable_corpus[idx],
        'sentence': original_sentences[idx],
        'pagerank_score': pagerank_scores.get(hashable_corpus[idx], 0)
    })

print(final_documents)
final_documents.sort(key=lambda doc: doc['pagerank_score'], reverse=True)

for document in final_documents:
    print(document['pagerank_score'], document['sentence'])

'''
ratio=0.2

most_important_docs = [list(doc) for doc in hashable_corpus[:int(len(corpus) * ratio)]]

sentences_by_corpus = dict(zip(hashable_corpus, original_sentences))
important_sentences = [sentences_by_corpus[tuple(important_doc)] for important_doc in most_important_docs]

important_sentences.sort(key=lambda s: s.index)

print('Selected own implementation sentences', [sentence.text for sentence in important_sentences])
'''
