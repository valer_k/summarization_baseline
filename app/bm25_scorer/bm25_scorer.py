from app.preprocessor.preprocessor import Preprocessor
from app.preprocessor.bag_of_words_model import bag_of_words_model

import math

import copy

class bm25_scorer:

    def __init__(self):

        self.PARAM_K1 = 1.5
        self.PARAM_B = 0.75
        self.EPSILON = 0.25

        self.sentences = []

    def get_scores(self, sentences):

        self.sentences = copy.deepcopy(sentences)

        corpus_size = len(self.sentences)
        df = {}
        idf = {}

        avgdl = sum(float(len(sentence['corpus'].keys())) for sentence in self.sentences) / corpus_size

        for sentence in self.sentences:

            sentence['document_length'] = len(sentence['corpus'])
            '''
            frequencies = {}
            
            for token in sentence['corpus'].keys():
                try:
                    frequencies[token] += 1
                except KeyError:
                    frequencies[token] = 1

            sentence['frequency_list'] = frequencies
            '''
            for token in sentence['corpus'].keys():
                try:
                    df[token] += 1
                except KeyError:
                    df[token] = 1

        for token in df.keys():
            '''
            idf[token] = math.log(corpus_size - df[token] + 0.5) - math.log(df[token] + 0.5)
            '''
            idf[token] = math.log(corpus_size / df[token])

        average_idf = sum(float(val) for val in idf.values()) / len(idf)

        for sentence in sorted(self.sentences, key=lambda sentence: sentence['position']):

            scores = []

            for sentence_to_compare in sorted(self.sentences, key=lambda sentence: sentence['position']):

                score = 0

                for token in sentence['corpus'].keys():

                    if token not in sentence_to_compare['corpus'].keys():
                        continue

                    '''
                    idf_value = idf[token] if idf[token] >= 0 else self.EPSILON * average_idf
                    '''
                    score += (idf[token] * sentence_to_compare['corpus'][token] * (self.PARAM_K1 + 1)
                              / (sentence_to_compare['corpus'][token] + self.PARAM_K1 * (
                                    1 - self.PARAM_B + self.PARAM_B * sentence_to_compare['document_length']  / avgdl)))
                scores.append(score)

            sentence['weights'] = scores


if __name__ == '__main__':

    example = "This week marks the final print copy of NME, once the engine of the hype and heat around the music industry." \
              " But its narrow musical focus in its later years doomed it to irrelevance. On one hand, the demise of " \
              "NME’s print edition seems inevitable. For all the title’s loud touting of its increased circulation " \
              "figures since it became a freesheet in September 2015 – they were apparently better than they had " \
              "been since 1964 – the paper was an irrelevant shadow of its former self. The kind of things about NME " \
              "that had once been hot topics had long ceased to be discussed, even within the music industry. " \
              "Who was on its cover that week? Who was it hyping as the new saviour of rock’n’roll? Which unfortunate " \
              "had been dealt a kicking in the reviews section by one of its bolshy star writers? If it was mentioned at " \
              "all, it was in tones of bafflement and pity. The last big furore NME caused wasn’t over an album it had " \
              "slagged off, nor a sacred cow it had taken to task in an interview, nor a band it had praised to the " \
              "point that said band was clearly doomed, incapable of fulfilling the expectations heaped on their shoulders." \
              " It was when its online wing inexplicably ran the news story: “M&S to stop selling £2.50 vegan ‘cauliflower " \
              "steaks’ following complaints.” You didn’t have to be one of those misty-eyed nostalgics for the paper’s " \
              "glory days, ever ready to trot out the impossibly well-worn stories about Nick Kent’s testicles hanging " \
              "out of his leather pants or Julie Burchill and Tony Parsons putting barbed wire round their area of the " \
              "office – stories we’re going to be given umpteen opportunities to enjoy yet again in the coming days – to" \
              " think this was a terribly depressing way for NME to go out. The truth is that the paper had umpteen " \
              "“golden eras”, and, with the greatest of respect to everyone involved in its manufacture, their existence" \
              " usually had less to do with the quality of the writing than whether or not you were 17 or 18 when you " \
              "were reading it. But regardless of whether you thought the NME’s best days involved prog or punk or Pete " \
              "Doherty, in the end it became clueless as to what it was supposed to be doing, or who it was supposed " \
              "to be for. But there is also something surprising about NME’s demise. Even its loudest detractor would " \
              "struggle to claim that it was anything other than adaptable. It had survived everything, from the rise " \
              "of rock’n’roll itself, four years after it had changed its title from the Musical Express (“incorporating " \
              "Accordion Times”), to the death of most of its rivals: it’s 18 years since Melody Maker and Select closed, " \
              "27 years since Sounds breathed its last. NME hadn’t always been at the forefront of every new genre that " \
              "appeared, and some had caused ructions and fissures to appear among its staff. Not everyone on the NME " \
              "staff was as welcoming of punk as its then editor, Neil Spencer, who fixed the 1976 readers’ poll so " \
              "that the Sex Pistols were the “brightest hope for 1977”, rather than Phil Collins’s jazz-fusion band " \
              "Brand X; a so-called “hip-hop war” erupted in the mid-80s between writers who wanted to focus on rap, soul " \
              "and club music and those who preferred to stick with what had become NME’s stock-in-trade, indie guitar " \
              "rock. But none of them actually brought about its downfall. So what did? The pat answer is the internet, " \
              "which dealt a mortal blow to print media generally, and ushered in an era where pop and rock music has ceased" \
              " to be, in the writer Jon Savage’s lovely phrase, “the teenage news” – the main means by which youth " \
              "culture defines itself, a conduit that transmitted everything from fashion to politics to a young audience. " \
              "Accordingly, music taste has become less tribal and more pluralistic than before. If there’s always some " \
              "die-hard numpty eager to get up a petition when Glastonbury books a rapper or R&B star as a headliner, there’s " \
              "a sense that most music fans don’t define themselves in narrow terms: the kind of clannish boundaries " \
              "that could once have caused a hip-hop war to erupt among NME staff no longer exist. In the 00s, NME’s " \
              "response to this was to double down on its core audience, focusing increasingly exclusively on indie " \
              "guitar rock, then enjoying a renaissance thanks to the White Stripes and the Strokes. Doing so prompted " \
              "a renaissance in sales, but when that wave of “alternative” rock ran out of steam, NME found itself without " \
              "any real musical constituency to speak of. By the turn of the decade there was something very off-putting " \
              "about how many of its covers were devoted to artists who were dead, or bands that had long split up, or " \
              "enjoyed their biggest success decades ago: the Smiths, the Clash, Syd Barrett, John Lennon, Joy Division, " \
              "the Sex Pistols, the Stone Roses. The past is more interesting than the present, was the disheartening " \
              "underlying message. Perhaps if it had paid less attention to market research and kept its musical " \
              "outlook broad, it might have survived longer, better-equipped to navigate an era in which R&B and " \
              "hip-hop are commercially and creatively dominant, and grime is the underground genre enjoying the biggest " \
              "crossover into mainstream success. Or perhaps not: the precarious climate for print media, particularly " \
              "in the music press, doesn’t really encourage any kind of risk-taking. As it is, NME finds itself exiting " \
              "the stage mourned exclusively by people old enough to remember a time when it seemed important. Sad to say, " \
              "it seems unlikely you’ll find an 18-year-old in 2018 who cares much whether it exists or not."

    preproc = Preprocessor()

    preproc.preprocess(text=example)

    bow = bag_of_words_model()
    bow.build_model(preproc.sentences)

    scorer = bm25_scorer()
    scorer.get_scores(bow.sentences)