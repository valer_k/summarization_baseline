import json
import os

BASE_DIR_DAILYMAIL = '/home/valer/University/text_summarization/summarization_corpora/dailymail_stories/'
BASE_DIR_CNN = '/home/valer/University/text_summarization/summarization_corpora/cnn_stories/'

with open(file='./data/cnn_stories.json', mode='w', encoding='utf-8') as output_json:

    i = 0

    for file in os.listdir(BASE_DIR_CNN):

        with open(file=BASE_DIR_CNN + file, encoding='utf-8') as story_file:

            story_list = story_file.read().replace('\xa0', ' ').split('\n')

        summary = []
        story = []

        highlight_flag = 0

        for tokens in story_list:

            if tokens == '':
                continue

            elif tokens == '@highlight':
                highlight_flag = 1

            elif highlight_flag == 1:
                summary.append(tokens)
                highlight_flag = 0

            else:
                story.append(tokens)

        output_json.write(
            json.dumps({
                'story': story,
                'summary': summary
            }) + '\n'
        )

        i += 1

        print('Written story no.', i)

